/**
 * Dibuja un gráfico lobulado.
 *
 * @param {Float} diameter Diámetro del patrón a generar (eje radial).
 * @param {Float} count    Cantidad de lóbulos a dibujar.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Shapes/lobes.scad
 * @license CC-BY-NC-4.0
 */
module lobes(diameter, count, ratio = 0.1)
{
    function _fn(t) = let(_r = diameter / 2) (1 - ratio) * _r + ratio * _r * cos(count * t * 360);

    _fragments = $fn
        ? 2 * $fn
        : 360;

    polygon(
        points = [ for (_i = [ 0 : _fragments ])
            let(
                _angle = _i * 360 / _fragments,
                _r     = _fn(_i / _fragments)
            )
            _r * [ cos(_angle), sin(_angle) ]
        ]
    );
}
