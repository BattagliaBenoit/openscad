/**
 * Genera un contenedor para memorias SD y Micro-SD.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Cylinder/children/memory.scad
 * @license CC-BY-NC-4.0
 */
//---------------------------------------------------------------
use <../../../Functions/Utils/stlName.scad>
use <../../Memory/micro-sd.scad>
use <../../Memory/sd.scad>
use <../../Pattern/rectangular.scad>
use <./common.scad>
//---------------------------------------------------------------
/**
 * Calcula el tamaño de la memoria en función de si se debe rotar o no la memoria.
 */
function getMemorySize(memory, rotate) = [
    rotate ? memory[2] : memory[0],
    memory[1],
    rotate ? memory[0] : memory[2]
];

/**
 * Dibuja las filas y columnas de memorias.
 *
 * @param {Float[]} memory Configuración de los tamaños de la memoria a dibujar.
 * @param {Float}   length Longitud de la parte interna del contenedor..
 * @param {Boolean} rotate Si es `true` las memorias se rotan sobre el eje Z.
 * @param {Float[]} skip   Índice de las filas a omitir.
 */
module drawItemRows(diameter, length, thickness, memory, rotate = false, skip = [])
{
    _size   = getMemorySize(memory, rotate);
    _length = _size[1];
    _height = _size[2];
    _width  = _size[0];
    _sep    = thickness + _width;
    _limit  = floor((diameter / 2) / _sep);
    for (_n = [ - _limit : _limit ])
    {
        if (!search(_n, skip))
        {
            _x = _n * _sep;
            translate([ _x, 0, 0.01 ])
            {
                drawItemRow(diameter, length, thickness, memory, _x, rotate)
                {
                    children();
                }
            }
        }
    }
}

/**
 * Dibuja una fila de memorias.
 *
 * @param {Float}   diameter  Diámetro del cilindro.
 * @param {Float}   length    Longitud del cilindro (eje Z).
 * @param {Float}   thickness Grosor de las paredes del patrón.
 * @param {Float[]} memory    Configuración de los tamaños de la memoria a dibujar.
 * @param {Float}   x         Coordenada X donde se dibujará la fila.
 * @param {Boolean} rotate    Si es `true` las memorias se rotan sobre el eje Z.
 */
module drawItemRow(diameter, length, thickness, memory, x, rotate = false)
{
    _size   = getMemorySize(memory, rotate);
    _length = _size[1];
    _height = _size[2];
    _width  = _size[0];
    _max    = cylinderCalc(diameter, abs(x) + _width / 2) - _height;
    if (_max > 0)
    {
        rectangular(_width, _width, _height, thickness, _max)
        {
            translate([ 0, 0, length - _length / 2 ])
            {
                cube([ _width, _height, _length ], center = true);
            }
            translate([ rotate ? _width / 2 : - _width / 2, - _height / 2, length ])
            {
                rotate([ -90, 0, rotate ? 90 : 0 ])
                {
                    children();
                }
            }
        }
    }
}

/**
 * Dibuja el contenedor para las memorias especificadas.
 *
 * @param {Float}   diameter       Diámetro del cilindro.
 * @param {Float}   length         Longitud del cilindro (eje Z).
 * @param {Float}   thickness      Grosor de las paredes del patrón.
 * @param {Float}   tolerance      Tolerancia a usar para dejar holgura en las ranuras de las memorias.
 * @param {Boolean} rotateSd       Si es `true` las memorias SD se rotan sobre el eje Z.
 * @param {Float[]} skipSd         Índices de las filas de memorias SD a omitir.
 * @param {Boolean} rotateMicroSd  Si es `true` las memorias MicroSD se rotan sobre el eje Z.
 * @param {Float[]} skipMicroSd    Índices de las filas de memorias MicroSD a omitir.
 * @param {Float}   margin         Margen a dejar entre la parte superior de la memoria y el cilindro para poder extraerla con facilidad.
 * @param {String}  name           Nombre del modelo a generar.
 */
module cylinderMemory(diameter, length, thickness, tolerance = 0.6, rotateSd = true, skipSd = [], rotateMicroSd = true, skipMicroSd = [], margin = 5, name = "cylinder-memory")
{
    _microSd = getMicroSdSize(tolerance = tolerance);
    _sd      = getSdSize(tolerance = tolerance);
    _length  = length == undef
        ? max(skipMicroSd == true ? 0 : _microSd[1], skipSd == true ? 0 : _sd[1])
        : length;
    if (_length > 0)
    {
        difference()
        {
            echo(stlName(name, [ diameter, _length, thickness, rotateSd, skipSd, rotateMicroSd, skipMicroSd, margin ]));
            cylinder(d = diameter, h = _length - margin);
            if (skipSd != true)
            {
                drawItemRows(diameter, max(_length, _sd[1]), thickness, _sd, rotateSd, skipSd)
                {
                    color("Orange", 0.5)
                    {
                        %sd(tolerance = tolerance);
                    }
                }
            }
            if (skipMicroSd != true)
            {
                drawItemRows(diameter, max(_length, _microSd[1]), thickness, _microSd, rotateMicroSd, skipMicroSd)
                {
                    color("Lime", 0.5)
                    {
                        %microSd(tolerance = tolerance);
                    }
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------
// Contenedor para un solo tipo de memoria.
//-----------------------------------------------------------------------------
// memory(skipSd      = true, rotateMicroSd = false);
// memory(skipSd      = true, rotateMicroSd = true);
// memory(skipMicroSd = true, rotateMicroSd = true);
//-----------------------------------------------------------------------------
// Contenedor para ambos tipos de memorias.
// Agregar/omitir  el número de las filas que se solapan en las variables skipSd y skipMicroSd..
//-----------------------------------------------------------------------------
// memory(
//     tolerance     = tolerance,
//     rotateSd      = rotateSd,
//     skipSd        = [ ... ],
//     rotateMicroSd = rotateMicroSd,
//     skipMicroSd   = [ ... ]
// );
