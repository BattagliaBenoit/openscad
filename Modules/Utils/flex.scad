/**
 * Repite una forma 2D tantas veces como se especifique sobre el eje X
 * y lo distribuye centrado.
 *
 * NOTA: Para un correcto centrado, el hijo debe estar centrado.
 *
 * @param {Float}     width  Ancho a distribuir (eje X).
 * @param {Integer}   count  Cantidad de elementos a distribuir.
 * @param {String[]}  labels Listado de etiquetas a colocar en cada elemento. Cada etiqueta está formada por:
 *                           - 0: Texto de la etiqueta.
 *                           - 1: Offset en Y para desplazar la etiqueta.
 *                           - 2: Tamaño de la fuente.
 * @param {Integer[]} skip   Índice de los elementos a omitir.
 */
module flexCenter(width, count = 2, labels = [], skip = [])
{
    if (count > 1)
    {
        _dx = width / (count - 1);
        for (_x = [ 0 : count - 1 ])
        {
            if (!search(_x, skip))
            {
                translate([ - width / 2 + _x * _dx, 0, 0 ])
                {
                    children();
                    _label = labels[_x];
                    if (_label)
                    {
                        translate([ 0, _label[1] ])
                        {
                            text(
                                _label[0],
                                halign="center",
                                valign="center",
                                size=_label[2] ? _label[2] : 4
                            );
                        }
                    }
                }
            }
        }
    }
    else
    {
        children();
    }
}
