/**
 * Módulos para dibujar cajas con bordes redondeados.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Box/rounded.scad
 * @license CC-BY-NC-4.0
 */
 //----------------------------------------------------------
 use <./frame.scad>
 //----------------------------------------------------------
/**
 * Genera las coordenadas para dibujar un rectángulo con las esquinas redondeadas.
 * Cada esquina puede tener un radio diferente.
 *
 * @param {Float}   width  Ancho del rectángulo.
 * @param {Float}   height Alto del rectángulo.
 * @param {Float[]} radius Radios a usar para redondear cada esquina.
 */
function boxRoundedRect(width, height, radius = [ 1, 1, 1, 1 ]) = let(
        _r = is_list(radius)
            ? radius
            : [ radius, radius, radius, radius ]
    )
    [
        [
            [             0, height - _r[0] ],
            [         _r[0], height - _r[0] ],
            [         _r[0], height         ],
            [ width - _r[1], height         ],
            [ width - _r[1], height - _r[1] ],
            [ width        , height - _r[1] ],
            [ width        ,          _r[2] ],
            [ width - _r[2],          _r[2] ],
            [ width - _r[2],              0 ],
            [         _r[3],              0 ],
            [         _r[3],          _r[3] ],
            [             0,          _r[3] ]
        ],
        [
            [         _r[0], height - _r[0] ],
            [ width - _r[1], height - _r[1] ],
            [ width - _r[2],          _r[2] ],
            [         _r[3],          _r[3] ]
        ]
    ];
//----------------------------------------------------------
/**
 * Dibuja un bloque con los bordes XY redondeados.
 *
 * @param {Float}         width   Ancho del bloque (eje X).
 * @param {Float}         height  Largo del bloque (eje Y).
 * @param {Float}         length  Alto del bloque (eje Z).
 * @param {Float|Float[]} radius  Radios del borde de cada esquina.
 * @param {Boolean[]}     zradius Indica si se redondean los bordes del eje Z ([ top, bottom ]).
 */
module boxRounded(width, height, length, radius = [ 1, 1, 1, 1 ], zrounded = [], center = true)
{
    _c  = 0.5 * [ width, height, length ];
    _tr = center ? - _c : [ 0, 0, 0 ];
    translate(_tr)
    {
        difference()
        {
            if (radius)
            {
                _rmax = is_list(radius)
                    ? max(radius)
                    : radius;
                linear_extrude(length, convexity = 10)
                {
                    if (_rmax > height / 2 || _rmax > width / 2)
                    {
                        // Si el radio de redondeo sobrepasa el ancho o el alto, lo limitamos al tamaño requerido.
                        intersection()
                        {
                            square([ width, height ]);
                            boxRounded2d(width, height, radius);
                        }
                    }
                    else
                    {
                        boxRounded2d(width, height, radius);
                    }
                }
            }
            else
            {
                cube([ width, height, length ], center = center);
            }
            if (zrounded)
            {
                _r = is_num(radius) ? radius : radius[0];
                if (_r)
                {
                    translate(_c)
                    {
                        for (_z = [ 0, 1 ])
                        {
                            if (zrounded[_z])
                            {
                                mirror([ 0, 0, _z ])
                                {
                                    translate([ 0, 0, - length / 2 ])
                                    {
                                        boxRoundedBorder(width, height, _r);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
//----------------------------------------------------------
/**
 * Dibuja un bloque con los bordes XY redondeados.
 *
 * @param {Float}         width  Ancho del bloque (eje X).
 * @param {Float}         height Largo del bloque (eje Y).
 * @param {Float|Float[]} radius Radios del borde de cada esquina.
 */
module boxRounded2d(width, height, radius = [ 1, 1, 1, 1 ])
{
    // Si se pasa un número, lo convertimos a un array.
    _radius = is_list(radius)
        ? radius
        : [ radius, radius, radius, radius ];
    _data = boxRoundedRect(width, height, _radius);
    polygon(points = _data[0]);
    for (_index = [ 0 : 3 ])
    {
        if (_radius[_index] > 0)
        {
            translate(_data[1][_index])
            {
                circle(r = _radius[_index]);
            }
        }
    }
}
//----------------------------------------------------------
/**
 * Dibuja un bloque con todos los bordes redondeados.
 *
 * @param width  Ancho del bloque (eje X).
 * @param height Largo del bloque (eje Y).
 * @param length Alto del bloque (eje Z).
 * @param radius Radio del borde.
 */
module boxRounded3d(width, height, length, radius = 1)
{
    if (radius > 0)
    {
        hull()
        {
            for (z = [ -1, 1 ])
            {
                for (y = [ -1, 1 ])
                {
                    for (x = [ -1, 1 ])
                    {
                        translate([
                            x * (width  / 2 - radius),
                            y * (height / 2 - radius),
                            z * (length / 2 - radius)
                        ])
                        {
                            sphere(r = radius);
                        }
                    }
                }
            }
        }
    }
    else
    {
        cube([ width, height, length ], center = true);
    }
}

/**
 * Dibuja un marco para redondear la parte inferior y superior.
 *
 * @param {Float} width  Ancho del marco.
 * @param {Float} height Alto del marco.
 * @param {Float} radius Radio de las esquinas.
 */
module boxRoundedBorder(width, height, radius)
{
    _d = 2 * radius;
    translate([ 0, 0, radius ])
    {
        difference()
        {
            translate([ 0, 0, - _d / 2 ])
            {
                cube([ width + _d, height + _d, _d ], center = true);
            }
            cube([ width - _d, height - _d, 3 * _d ], center = true);
            boxFrame(width, height, radius);
        }
    }
}

/**
 * Dibuja un bloque hueco con los bordes XY redondeados.
 *
 * @param {Float} width   Ancho del bloque (eje X).
 * @param {Float} height  Largo del bloque (eje Y).
 * @param {Float} length  Alto del bloque (eje Z).
 * @param {Float} radius  Radios del borde de cada esquina.
 */
module boxRoundedHollow(width, height, length, thickness, radius = undef, center = false)
{
    _2t = 2 * thickness;
    _h  = height + _2t;
    _w  = width  + _2t;
    _r  = radius == undef
        ? thickness
        : radius;
    _o  = is_list(_r)
        ? [ for (_n = _r) _n == 0 ? 0 : abs(_n) + thickness ]
        : thickness > 0 && _r > 0
            ? _r + thickness
            : 0;
    _i  = is_list(_r)
        ? [ for (_n = _r) _n < 0 ? 0 : _n ]
        : _r;
    difference()
    {
        boxRounded(_w, _h, length + thickness, _o, center = center);
        _c = center
            ? [ 0, 0, thickness / 2 ]
            : [ thickness, thickness, thickness ];
        translate(_c)
        {
            boxRounded(width, height, length + 0.01, _i, center = center);
        }
    }
}
