use <./container.scad>
use <./constants.scad>
//---------------------------------------------------------------
constants = getConstants();
INNER     = constants[0];
LENGTH    = constants[2];
THICKNESS = constants[3];
RADIUS    = constants[5];
DIAMETER  = 1.30; // Diámetro del cable dupont.
//---------------------------------------------------------------
/**
 * Dibuja el soporte de cables dupont a introducir dentro del contenedor.
 *
 * @param {Float}   length    Altura de los soportes (eje Z).
 * @param {Float}   thickness Grosor de las paredes de cada soporte.
 * @param {Float}   diameter  Diámetro del cable dupont a usar.
 * @param {Integer} count     Cantidad de arcos a dibujar.
 * @param {Float[]} angles    Ángulos donde se colocarán los soportes.
 */
module dupont(length = LENGTH, thickness = THICKNESS, diameter = DIAMETER, count = 7, angles = [ [0, 40], [120,160], [240,280] ])
{
    _dt = thickness * 1.5;                // Diámetro del toroide superior usado como tope.
    _dr = _dt + diameter;                 // Separación entre arcos
    _r  = (INNER - (_dt + diameter)) / 2; // Radio máximo de todo el conjunto.
    for (_angles = angles)
    {
        _aFrom = _angles[0];
        _aTo   = _angles[1];
        rotate([ 0, 0, _aFrom ])
        {
            rotate_extrude(angle = _aTo - _aFrom)
            {
                for (_i = [ 0 : count - 1 ])
                {
                    _R = _r - _dr * _i;
                    translate([ - thickness / 2, 0 ])
                    {
                        translate([ _R, (length - thickness) / 2 ])
                        {
                            square([ thickness, length - thickness ], center = true);
                        }
                        translate([ _R, length - _dt / 2 ])
                        {
                            circle(d = _dt);
                        }
                    }
                }
            }
        }
    }
}
//---------------------------------------------------------------
$fn       = 180;
length    = 25;
thickness = 1.5;
type      = "container";
if (type == "all")
{
    container(length)
    {
        dupont(length - 2 * THICKNESS, thickness);
    }
}
else if (type == "container")
{
    difference()
    {
        container(length);
        title("DUPONT", size = 6, valign = "baseline", z = length - 4.5);
    }
}
else if (type == "dupont")
{
    cylinder(d = INNER -  2 * THICKNESS, h = thickness);
    dupont(length - 2 * THICKNESS, thickness);
}
