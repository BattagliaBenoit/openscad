/**
 * Dibuja una caja para guardar brocas.
 * Modificando los valores de la variable `sizes` se puede incluso
 * usar para guardar otros tipos de elementos tales como lápices,
 * bolígrafos, pinceles, etc..
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Storage/DrillHolder.scad
 * @license CC-BY-NC-4.0
 * @see     http://www.thingiverse.com/thing:2095807
 */
//-----------------------------------------------------------------------------
use <../Functions/Array/pluck.scad>
use <../Modules/Box/rounded.scad>
use <../Modules/Box/separator.scad>
//-----------------------------------------------------------------------------
// Valores personalizables
//-----------------------------------------------------------------------------
bits      =  0;   // Altura de los soportes para las puntas (0 para no dibujarlos).
lid       =  3;   // Altura de la parte de la tapa que entra en la caja
notch     = 11.2; // Altura de la muesta para sacar las brocas más fácilmente.
show      =  3;   // 1: Caja, 2: Tapa, 3: Caja y tapa.
thickness =  1.2; // Grosor de las paredes.
tolerance =  0.8; // Valor a usar como holgura.
// Dimensiones de las brocas: [0]: Diámetro, [1]: Largo
// Se pueden poner en cualquier orden pero si se ponen de mayor a menor
// se aprovecha el espacio que queda para guardar tornillos, puntas, etc.
sizes = [
    [ 10, 133 ],
    [  8, 117 ],
    [  6,  93 ],
    [  5,  86 ],
    [  4,  74 ],
    [  3,  61 ]
];
//-----------------------------------------------------------------------------
// Fin de la personalización.
//-----------------------------------------------------------------------------
// Valores calculados.
//-----------------------------------------------------------------------------
eps       = $preview ? 0.01 : 0;
diameters = arrayPluck(sizes, 0);                   // Diámetros de las brocas.
lengths   = arrayPluck(sizes, 1);                   // Longitudes de las brocas.
count     = len(sizes);                             // Cantidad de elementos
height    = max(diameters) + tolerance + lid;       // Altura interna de la caja (eje Z)
length    = max(lengths) + tolerance;               // Largo interno de la caja (eje Y)
width     = offsetX(count, thickness) - thickness;  // Anchura interna de la caja (eje X)
//-----------------------------------------------------------------------------
function offsetX(i, thickness = 0, total = 0) = i > 0
    ? offsetX(i - 1, thickness, total + sizes[i - 1][0] + thickness + tolerance)
    : total;
//-----------------------------------------------------------------------------
// Inicio
//-----------------------------------------------------------------------------
/**
 * Dibuja la cuadrícula para colocar las puntas.
 *
 * @param {Float} z         Altura de la pared (eje Z).
 * @param {Float} diameter  Diámetro de la punta (eje X/Y).
 * @param {Float} margin    Margen a dejar entre puntas.
 * @param {Float} tolerance Tolerancia a usar para que la tapa encaje fácilmente.
 */
module bits(z = 25.4, diameter = 25.4 * (1/4), margin = thickness, tolerance = tolerance)
{
    _s = diameter + tolerance;
    _d = _s / cos(30);
    linear_extrude(z, convexity = 10)
    {
        for(_x = [ width - _s + thickness : - (_s + thickness) : 0 ])
        {
            for(_y = [ length - _d + thickness : - (_d + thickness) : 0 ])
            {
                translate([ _x, _y ])
                {
                    square([ _s, _d ]);
                }
            }
        }
    }
}

/**
 * Dibuja la caja para almacenar las brocas.
 *
 * @param {Float} width     Ancho de la caja (eje X).
 * @param {Float} length    Largo de la caja (eje Y).
 * @param {Float} height    Alto de la caja (eje Z).
 * @param {Float} thickness Grosor de las paredes.
 * @param {Float} lid       Grosor de la parte inferior de la tapa que se introduce en la caja.
 */
module box(width, length, height, thickness, lid = lid)
{
    _2t = 2 * thickness;
    _h  = height - lid;
    boxRoundedHollow(width, length, height + thickness, thickness);
    difference()
    {
        drills(_h, true);
        if (notch > _2t)
        {
            // Muesca para sacar las brocas fácilmente.
            _dy = min(lengths);
            _rs = thickness; // Radio de redondeo de los bordes de separador.
            _z  = min(_h, notch);
            translate([ _2t, _rs + _dy / 4, _h - _z + 2 * thickness ])
            {
                rotate([ 90, 0, 90 ])
                {
                    boxSeparator(_dy / 2, _z, width - _2t, _rs);
                }
            }
        }
    }
    if (bits)
    {
        _ch = min(_h, bits) + thickness;
        _cl = length + _2t;
        _cw = width  + _2t;
        translate([ 0, 0, thickness ])
        {
            difference()
            {
                boxRounded(_cw, _cl, _ch - eps, 2 * thickness, center = false);
                drills(_ch, false);
                bits(_ch);
            }
        }
    }
}

/**
 * Dibuja la tapa de la caja.
 *
 * @param {Float} width     Ancho de la caja (eje X).
 * @param {Float} length    Largo de la caja (eje Y).
 * @param {Float} thickness Grosor de la parte superior de la tapa.
 * @param {Float} lid       Grosor de la parte inferior de la tapa que se introduce en la caja.
 * @param {Float} tolerance Tolerancia a usar para que la tapa encaje fácilmente.
 */
module cover(width, length, thickness, lid = lid, tolerance = tolerance)
{
    _2t = 2 * thickness;
    _l  = length + _2t;
    _w  = width + _2t;
    difference()
    {
        union()
        {
            // Bloque externo
            boxRounded(_w, _l, thickness, 2 * thickness, center = false);
            // Bloque interno
            translate([ thickness + tolerance / 2, thickness + tolerance / 2, thickness ])
            {
                boxRounded(width - tolerance, length - tolerance, lid, 3 * thickness, center = false);
            }
        }
        _t = thickness / 2;
        for (_x = [ _t - eps, _w - _t + eps ])
        {
            // Muescas lado largo
            translate([ _x, _l / 2, thickness + eps ])
            {
                cube([ thickness, min(_l / 4, 20), thickness ], center = true);
            }
        }
    }
}

/**
 * Dibuja las secciones para colocar las brocas.
 *
 * @param {Float}   height    Altura de la sección (eje Z).
 * @param {Boolean} hollow    Si es `true` se dibujan las secciones huecas en vez de sólidas.
 * @param {Float}   thickness Grosor de las parendes.
 * @param {Float}   tolerance Tolerancia a usar para que la tapa encaje fácilmente.
 */
module drills(height, hollow = false, thickness = thickness, tolerance = tolerance)
{
    _r = hollow ? thickness : 2 * thickness;
    for (_i = [ 0 : count - 1 ])
    {
        translate([ offsetX(_i, thickness), 0, thickness ])
        {
            _size = sizes[_i];
            _w    = _size[0] + tolerance;
            _l    = _size[1] + tolerance;
            _b    = _i == 0
                ? [ _r, 0, 0, _r ]
                : _i == count - 1
                    ? [ 0, max(lengths) == _size[1]    ? _r : 0, _r, 0 ]
                    : [ 0, sizes[_i - 1][1] > _size[1] ? _r : 0,  0, 0 ];
            if (hollow)
            {
                boxRoundedHollow(_w, _l, height, thickness, _b);
            }
            else
            {
                boxRounded(_w + 2 * thickness, _l + 2 * thickness, height, _b, center = false);
            }
        }
    }
}
//-----------------------------------------------------------------------------
// Inicio del módulo
//-----------------------------------------------------------------------------
$fn = 120;
if (show == 1 || show == 3)
{
    box(width, length, height, thickness);
}
if (show == 2 || show == 3)
{
    translate([ width + 5, 0 , 0 ])
    {
        cover(width, length, thickness);
    }
}
