/**
 * Genera un contenedor para memorias SD y Micro-SD.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Storage/PlayingCards.scad
 * @license CC-BY-NC-4.0
 * @see     https://www.thingiverse.com/thing:4151153
 */
//-----------------------------------------------------------------------------
use <../Functions/Array/join.scad>
use <../Functions/Utils/stlName.scad>
use <../Modules/Box/rounded.scad>
//-----------------------------------------------------------------------------
// Tamaños predefinidos ([x, y, z, label]).
//-----------------------------------------------------------------------------
sizes = [
    // x,  y,  z
    [ 62, 95, 15, [ "FOURNIER", "#12" ] ],
    [ 63, 88, 19, [ "CAYRO", "POKER" ] ],
    [ 65, 95, 33, [ "BLINK" ] ]  // Plastificadas con fundas 65 x 95
];
//-----------------------------------------------------------------------------
size      =  0;         // Índice del tamaño predefinido.
margin    =  0.8;       // Margen a dejar de cada lado de la carta dentro de la caja.
width     = sizes[size][0] + 2 * margin;
length    = sizes[size][1] + 2 * margin;
height    = sizes[size][2] + margin;
border    =  3;         // Radio para redondear los bordes de las esquinas.
diameter  = 20;         // Diámetro de la muesca para sacar las cartas.
fsize     =  8;         // Tamaño de la fuente.
fmargin   =  2;         // Margen a dejar entre líneas para las etiquetas de varias líneas.
notchs    = ["x", "y"]; // Ejes de las muescas para sacar las cartas.
thickness =  1.6;       // Grosor de las paredes.
tolerance =  0.6;       // Tolerancia a dejar entre la tapa y la base.
//-----------------------------------------------------------------------------
// Valores calculados.
//-----------------------------------------------------------------------------
$fn = 120;
eps = $preview ? 0.01 : 0;
b   = thickness;
t   = 2 * thickness;
w   = width  + 2 * (t + tolerance); // Ancho de la base y de la tapa (eje x).
l   = length + 2 * (t + tolerance); // Largo de la base y de la tapa (eje y).

/**
 * Dibuja la base del contenedor.
 */
module base()
{
    // Base inferior que hace de tope
    translate([ 0, 0, - height / 2 - thickness / 2])
    {
        boxRounded(w, l, thickness, b);
    }
    // Base con las paredes.
    difference()
    {
        boxRounded(width + t, length + t, height, border + thickness);
        boxRounded(width, length, height + eps, border);
        for (_n = notchs)
        {
            if (_n == "x")
            {
                notch(width + 2 * t);
            }
            else if (_n == "y")
            {
                rotate([ 0, 0, 90 ])
                {
                    notch(length + 2 * t);
                }
            }
        }
    }
}

/**
 * Dibuja la tapa del contenedor.
 */
module cover()
{
    _h = height + thickness;
    difference()
    {
        translate([ 0, 0, - thickness / 2 ])
        {
            boxRounded(w, l, _h, b);
        }
        translate([ 0, 0, thickness / 2 ])
        {
            boxRounded(w - t, l - t, _h, max(0, b - thickness));
        }
    }
}

/**
 * Dibuja la etiqueta del contenedor.
 */
module label(text, top = true, side = true, halign = "center", valign = "center", size = 10, font = "Marmelad")
{
    if (top)
    {
        translate([ 0, 0, - height / 2 + 0.4 ])
        {
            rotate([ 0, 180, 0 ])
            {
                linear_extrude(thickness, convexity = 10)
                {
                    text(text, halign = halign, valign = valign, size = size, font = font);
                }
            }
        }
    }
    if (side)
    {
        translate([ w / 2 - 0.2, 0, - thickness / 2 ])
        {
            rotate([ 90, 180, 90 ])
            {
                linear_extrude(thickness, convexity = 10)
                {
                    text(text, halign = halign, valign = valign, size = size, font = font);
                }
            }
        }
    }
}

/**
 * Dibuja la muesca en la base para poder sacar las cartas.
 */
module notch(length, diameter = diameter)
{
    if (diameter > 0)
    {
        _z = height - diameter / 2;
        translate([ length / 2, 0, (diameter - height) / 2 + eps ])
        {
            rotate([ 0, -90, 0 ])
            {
                linear_extrude(length, convexity = 10)
                {
                    translate([ _z / 2, 0 ])
                    {
                        square([ _z, diameter ], center = true);
                    }
                    difference()
                    {
                        translate([ _z, 0 ])
                        {
                            square([ 2 * border, diameter + 2 * border ], center = true);
                        }
                        for (_x = [ -1, 1 ])
                        {
                            translate([ _z - border / 2, _x * (diameter + border) / 2 ])
                            {
                                translate([ - border / 2, _x * border / 2 ])
                                {
                                    circle(border);
                                }
                            }
                        }
                    }
                    circle(d = diameter);
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------
// Inicio del script.
//-----------------------------------------------------------------------------
echo(stlName("PlayingCards", [ sizes[size], margin ]));
base();
translate([ width + thickness + 8, 0, 0 ])
{
    difference()
    {
        cover();
        labels = sizes[size][3];
        if (labels)
        {
            fy     = len(labels) * (fsize + fmargin) - fmargin;
            for (l = [ 0 : len(labels) - 1 ])
            {
                translate([ 0, fy / 4 - l * (fsize + fmargin), - thickness ])
                {
                    label(labels[l], size = fsize, side = false);
                }
            }
            label(arrayJoin(labels, " "), size = fsize, top = false);
        }
    }
}

